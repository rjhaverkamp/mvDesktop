package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"

	filetype "gopkg.in/h2non/filetype.v1"
)

const dir = "/Users/rjrhaverkamp/Desktop"
const home = "/Users/rjrhaverkamp"

func main() {
	kq, err := syscall.Kqueue()
	if err != nil {
		log.Println("Error creating Kqueue descriptor!")
		return
	}
	// open folder
	fd, err := syscall.Open(dir, syscall.O_RDONLY, 0)
	if err != nil {
		log.Println("Error opening folder descriptor!")
		return
	}
	// build kevent
	ev1 := syscall.Kevent_t{
		Ident:  uint64(fd),
		Filter: syscall.EVFILT_VNODE,
		Flags:  syscall.EV_ADD | syscall.EV_ENABLE | syscall.EV_ONESHOT,
		Fflags: syscall.NOTE_DELETE | syscall.NOTE_WRITE | syscall.NOTE_EXTEND,
		Data:   0,
		Udata:  nil,
	}
	// configure timeout
	timeout := syscall.Timespec{
		Sec:  0,
		Nsec: 0,
	}
	// wait for events
	for {
		// create kevent
		events := make([]syscall.Kevent_t, 10)
		nev, err := syscall.Kevent(kq, []syscall.Kevent_t{ev1}, events, &timeout)
		if err != nil {
			log.Println("Error creating kevent")
		}
		// check if there was an event
		for i := 0; i < nev; i++ {
			// log
			log.Printf("Event [%d] -> %+v", i, events[i])
			go processFile()
		}
	}

}

func processFile() {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		go moveFile(file.Name())
	}
}

func moveFile(filename string) {
	buf, _ := ioutil.ReadFile(fmt.Sprintf("%s/%s", dir, filename))
	kind, unknown := filetype.Match(buf)
	if unknown != nil {
		fmt.Printf("Unknown: %s\n", unknown)
	}
	switch {
	case kind.MIME.Type == "image":
		err := os.Rename(fmt.Sprintf("%s/%s", dir, filename), fmt.Sprintf("%s/%s/%s", home, "Pictures", filename))
		if err != nil {
			log.Println(err)
		}

	case kind.MIME.Type == "video":
		err := os.Rename(fmt.Sprintf("%s/%s", dir, filename), fmt.Sprintf("%s/%s/%s", home, "Movies", filename))
		if err != nil {
			log.Println(err)
		}

	case kind.MIME.Type == "audio":
		err := os.Rename(fmt.Sprintf("%s/%s", dir, filename), fmt.Sprintf("%s/%s/%s", home, "Music", filename))
		if err != nil {
			log.Println(err)
		}
	default:
		err := os.Rename(fmt.Sprintf("%s/%s", dir, filename), fmt.Sprintf("%s/%s/%s", home, "Downloads", filename))
		if err != nil {
			log.Println(err)
		}
	}
	// fmt.Printf("File type: %s. MIME: %s\n", kind.Extension, kind.MIME.Value)
}
